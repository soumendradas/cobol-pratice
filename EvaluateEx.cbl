       IDENTIFICATION DIVISION.
       PROGRAM-ID. EVALUATE-EX.
       AUTHOR. SOUMENDRA DAS.
       
       DATA DIVISION.
       WORKING-STORAGE SECTION.
        01 A PIC 9(2).
       
       PROCEDURE DIVISION.
           
           DISPLAY "ENTER ANY NUMBER(1-5): ".
           ACCEPT A.
           EVALUATE A
               WHEN 1
                   DISPLAY "THE NUMBER IS ONE"
               WHEN 2
                   DISPLAY "THE NUMBER IS TWO"
               WHEN 3
                   DISPLAY "THE NUMBER IS THREE"
               WHEN 4
                   DISPLAY "THE NUMBER IS FOUR"
               WHEN 5
                   DISPLAY "THE NUMBER IS FIVE"
               WHEN OTHER
                   DISPLAY "INVALID NUMBER"
           
           STOP RUN.

       END PROGRAM EVALUATE-EX.
        