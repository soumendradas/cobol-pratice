       
       IDENTIFICATION DIVISION.
       PROGRAM-ID. MemorySavingDemoRead.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT EMPLOYEE-DATA ASSIGN TO "EmployeeData.DAT"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD EMPLOYEE-DATA.
       COPY "EmployeeCopyBook.cpy".

       WORKING-STORAGE SECTION.
       COPY "EmployeeCopyBook.cpy"
        REPLACING Employee-Rec BY WS-EMP-REC.
       01 WS-EOF PIC A VALUE SPACE.

       PROCEDURE DIVISION.
           OPEN INPUT EMPLOYEE-DATA.
           PERFORM UNTIL WS-EOF = 'Y'
               READ EMPLOYEE-DATA INTO WS-EMP-REC
               AT END MOVE 'Y' TO WS-EOF
               NOT AT END 
               IF WORKING OF WS-EMP-REC
                   DISPLAY EMP-ID OF WS-EMP-REC " "
                    EMP-NAME OF WS-EMP-REC
               END-READ
           END-PERFORM.
           CLOSE EMPLOYEE-DATA.
           STOP RUN.


       