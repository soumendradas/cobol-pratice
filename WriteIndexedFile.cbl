       IDENTIFICATION DIVISION.
       PROGRAM-ID. WriteIndexedFile.


       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT MyIndexedFile ASSIGN TO "myindexedfile.DAT"
           ORGANIZATION IS INDEXED
           ACCESS MODE IS RANDOM
           RECORD KEY IS Student-ID.

       DATA DIVISION.
       FILE SECTION.
       FD MyIndexedFile.

       01 Student-Record.
           02 Student-ID PIC 9(5).
           02 Student-Name PIC X(30).
           02 Student-Dept PIC X(20).

       WORKING-STORAGE SECTION.
       01 StudentKey PIC 9(5) VALUE 12345.
       01 StudentNameKey PIC X(30) VALUE "John Doe".

       PROCEDURE DIVISION.
           OPEN I-O MyIndexedFile

           PERFORM AddRecords
           PERFORM ReadRecords
      *    PERFORM DeleteRecord

           CLOSE MyIndexedFile.
           
           STOP RUN.

       AddRecords.
           MOVE 12345 TO Student-ID
           MOVE "John Doe" TO Student-Name
           MOVE "Computer Science" TO Student-Dept
           WRITE Student-Record INVALID KEY
               DISPLAY "Error writing record"
           END-WRITE

           MOVE 67890 TO Student-ID
           MOVE "Jane Smith" TO Student-Name
           MOVE "Mathematics" TO Student-Dept
           WRITE Student-Record INVALID KEY
               DISPLAY "Error writing record"
               NOT INVALID KEY DISPLAY "RECORD SAVED...."
           END-WRITE.

       ReadRecords.
           OPEN INPUT MyIndexedFile
           PERFORM VARYING Student-ID FROM 1 BY 1 UNTIL Student-ID > 99999
               READ MyIndexedFile
               INVALID KEY
                    EXIT PERFORM
               END-READ
               DISPLAY "Student ID: " Student-ID
               DISPLAY "Student Name: " Student-Name
               DISPLAY "Student Department: " Student-Dept
           END-PERFORM.
           CLOSE MYINDEXEDFILE.

      *DeleteRecord.
      *    MOVE StudentKey TO Student-Record.Student-ID
      *    DELETE Student-Record INVALID KEY
      *        DISPLAY "Error deleting record" END-DISPLAY
      *END-DELETE
      *IF Student-ID = 12345
      *        DISPLAY "Record with Student ID 12345 deleted" END-DISPLAY
      *    END-IF.
