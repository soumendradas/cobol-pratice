       IDENTIFICATION DIVISION.
       PROGRAM-ID. StudentObject.

       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01 StudentObject.
           02 Student-ID    PIC 9(5).
           02 Student-Name  PIC X(30).
           02 Student-Dept  PIC X(20).

       PROCEDURE DIVISION.
       
       Main-Program.
           SET StudentObject TO Student1
           DISPLAY "Student 1 - ID: " StudentObject.Student-ID
           DISPLAY "Student 1 - Name: " StudentObject.Student-Name
           DISPLAY "Student 1 - Department: " StudentObject.Student-Dept
       
           SET StudentObject TO Student2
           DISPLAY "Student 2 - ID: " Student-ID 
           DISPLAY "Student 2 - Name: " StudentObject.Student-Name
           DISPLAY "Student 2 - Department: " StudentObject.Student-Dept

           STOP RUN.

       Student1.
           MOVE 12345 TO Student-ID
           MOVE "John Doe" TO Student-Name
           MOVE "Computer Science" TO Student-Dept.

       Student2.
           MOVE 67890 TO Student-ID
           MOVE "Jane Smith" TO StudentObject.Student-Name
           MOVE "Mathematics" TO StudentObject.Student-Dept.
           