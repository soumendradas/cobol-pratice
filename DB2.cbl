       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBDBCR.
      *********************************************
      *PROGRAM TO CREATE DATABASE TABLES TO STORE*
      *AND ACCESS RECORDED DATA                                       *
      *********************************************
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
             EXEC SQL
           INCLUDE SQLCA
             END-EXEC.
       PROCEDURE DIVISION.
       MAIN-PARA.
           DISPLAY " SIMPLE PROGRAM TO CREATE A TABLE : "
                EXEC SQL
               CREATE TABLE INVESTOR ( IID INT , INAME CHAR(20),
               ISHARE SMALLINT ) IN DBMATE1.TSMATEKS
           END-EXEC.
           IF SQLCODE = 0
               DISPLAY " TABLE IS CREATED : "
           ELSE
               DISPLAY ' TABLE CREATION FAILED : ' SQLCODE
           END-IF.   
           STOP RUN. 