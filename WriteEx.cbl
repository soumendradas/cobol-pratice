       IDENTIFICATION DIVISION.
       PROGRAM-ID. WriteEx.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.
       OBJECT-COMPUTER.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT EmployeeData ASSIGN TO "EmployeeData.DAT"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD EmployeeData.
       01 Employee-Rec.
           05 EMP-ID PIC 9(04).
           05 FILLER PIC X(4) VALUE SPACES.
           05 EMP-NAME PIC X(20).


       WORKING-STORAGE SECTION.
       01 WS-EMP-ID PIC 9(4).
       01 WS-EMP-NAME PIC X(20).
       01 I PIC 9(02).

       PROCEDURE DIVISION.
       MAIN-PARA.
           PERFORM OPEN-FILE-PARA.
           PERFORM WRITE-PARA VARYING I FROM 1 BY 1 UNTIL I>2.
           perform CLOSE-FILE-PARA.

           STOP RUN.
       OPEN-FILE-PARA.
           OPEN OUTPUT EmployeeData.
       WRITE-PARA.

           DISPLAY "ENTER EMPLOYEE ID: ".
           ACCEPT WS-EMP-ID.

           DISPLAY "ENTER EMPLOYEE NAME: ".
           ACCEPT WS-EMP-NAME.

           MOVE WS-EMP-ID TO EMP-ID.
           MOVE WS-EMP-NAME TO EMP-NAME.
           WRITE EMPLOYEE-REC
           END-WRITE.

       CLOSE-FILE-PARA.
           CLOSE EmployeeData.
