       IDENTIFICATION DIVISION.
        PROGRAM-ID. BinarySearchExp.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 arr OCCURS 10 TIMES.
           05 item PIC 9(2).
       01 min PIC 9(2).
       01 max1 PIC 9(2).
       01 mid PIC 9(2).
       01 result PIC 9(2).

       PROCEDURE DIVISION.
           PERFORM INITIALIZATION
           PERFORM BINARY-SEARCH
           PERFORM DISPLAY-RESULT
           STOP RUN.

       INITIALIZATION.
           MOVE 2 TO arr(1)
           MOVE 5 TO arr(2)
           MOVE 7 TO arr(3)
           MOVE 10 TO arr(4)
           MOVE 23 TO arr(5)
           MOVE 34 TO arr(6)
           MOVE 36 TO arr(7)
           MOVE 54 TO arr(8)
           MOVE 66 TO arr(9)
           MOVE 70 TO arr(10)
           MOVE 0 TO min
           MOVE 9 TO max1.
           MOVE 5 TO mid.

       BINARY-SEARCH.
           PERFORM UNTIL min > max1
               COMPUTE mid = (min + max1) / 2
               IF arr(mid) = item
                    MOVE mid TO result
                    EXIT PERFORM
               ELSE
                   IF arr(mid) > item
                    MOVE mid - 1 TO max1
                   ELSE
                    MOVE mid + 1 TO min
               END-IF
           END-PERFORM.

       DISPLAY-RESULT.
           DISPLAY "Item found at index " result.
       END PROGRAM BinarySearchExp.
